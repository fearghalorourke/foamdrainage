import numpy as np
import math as mt # Very Sloppy
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.patches as mpatches
import matplotlib.animation as animation
from matplotlib.widgets import Slider,Button
import matplotlib, io, libconf
from datetime import datetime

#------------------------------------------------------------------------------------------------------------

'''
All the relevant files get read in
'''

filename = 'Beer_Animation_Array.npy'
Array_1 = np.load(filename)

filename = 'Beer_Increase_Height.npy'
Height_Array = np.load(filename)

filename = 'Plot_Heights_Array.npy'
Height_Index_Array = np.load(filename)


with io.open('config.cfg') as Conf_file:
    Con = libconf.load(Conf_file)
    
#------------------------------------------------------------------------------------------------------------

class AnimationClass:
    
    '''
    Class Animates a Previously determined Column Instance
    '''
    
    font = {'family' : 'serif', 'weight' : 'normal', 'size'   : 12} # Dictates what the text style on plots is
    matplotlib.rc('font', **font)
    
    def __init__(self,Sliderz,Animationz):
        '''
        Constructor sets up the animation and slider animation
        '''
        self.Relevant_Variables()
        self.Animation_Setup()
        if Sliderz:
            self.set_up_canvas_1()
            plt.show()
        if Animationz:
            self.set_up_canvas_2()
            self.For_Loop_That_Makes_Animation()
            self.Return_Animation()
            plt.show()
        end_time = datetime.now()
        print('Running Duration: {}'.format(end_time - start_time))
        
#------------------------------------------------------------------------------------------------------------

    def Physical_Quants(self):

        self.gamma = Con.gamma
        self.gravity = Con.gravity
        self.water_density = Con.water_density
        self.water_viscosity = Con.water_viscosity 
        self.bubdiam = Con.bubdiam
        self.bubble_velocity = Con.bubble_vel
        
        self.C_factor = Con.C_factor
        self.f_factor = Con.f_factor
        self.base_lf = Con.base_lf
        self.input_lf = Con.input_lf
        
        self.col_height = Con.col_height
        self.Glass_Height = Con.Glass_Height
        self.animate_separation = Con.animate_separation
        self.initial_lf = Con.initial_lf
        self.dt = Con.dt
        self.dz = Con.dz
        self.runtime = Con.Dimless_runtime
        self.maxtime = int (self.runtime / self.dt)
        
    def Relevant_Variables(self):

        self.Physical_Quants()
        self.firstdim = Array_1[0].size
        self.length_scale = mt.sqrt((self.C_factor*self.gamma)/(self.water_density*self.gravity))
        self.time_scale = 3.*self.f_factor*self.water_viscosity*(1./mt.sqrt(self.C_factor*self.gamma*self.water_density*self.gravity))
        self.phifactor = (5.35*self.length_scale*self.length_scale)/pow( ((4./3.)*mt.pi*(self.bubdiam * 0.5)**3), 2./3. )
        self.Dimless_Liq_Array = Height_Array
        self.Dimless_Foam_Array = Height_Index_Array
        
        self.Dimless_Total_Liquid_In_Foam = (self.col_height/self.length_scale)*(self.initial_lf/self.phifactor)
        self.z_height = self.col_height / (self.length_scale * self.dz)
        self.col_height = self.col_height/self.length_scale

    def Animation_Setup(self):
        
        '''
        Relevant for both the animation and slider
        '''
        
        self.Imagecounter = 0
        self.Image_One = []
        self.Image_Two = []
        self.first_temp = np.zeros((1,self.firstdim))
        self.x = np.linspace(0.,self.firstdim,self.firstdim)*self.length_scale*self.dz
        self.y = np.linspace(0.,.36,2)
        self.interval = 10
        self.delay = 100
        self.second_temp = np.zeros((Array_1[:,0].size,self.firstdim))
        self.pause = True
        self.Background_colour = '#bdf8a3'
        colours = [self.Background_colour,'#fffe7a','#b04e0f' ] #Setting up custom colour map
        self.cm = colors.LinearSegmentedColormap.from_list('My',colours,1000)
        
#------------------------------------------------------------------------------------------------------------
    
    '''
    The slider portion of the code
    '''
    
    def set_up_canvas_1(self):
        
        '''
        Set up for the slider canvas stuff
        '''
        
        fig1 = plt.figure() # Make figure 
        
        ax1 = plt.axes(xlim=(-.1,self.x[-1]),ylim=(0.0,.5))
        ax1.xaxis.set_major_locator(plt.NullLocator()) #This couple of lines makes partial x-ticks
        ax1.set_xticks(np.append(ax1.get_xticks(),np.linspace(0.,(self.firstdim)*self.length_scale*self.dz,5)))
        
        ax1.yaxis.set_major_locator(plt.NullLocator()) #This couple of lines makes partial y-ticks
        ax1.set_yticks(np.append(ax1.get_yticks(),np.linspace(0.,.36,7)))
        
        ax1.spines['left'].set_position('zero') #Puts the axis in the middle
        ax1.spines['right'].set_color(None)

        plt.xlabel('Height (m)',x = .7) # Labelling the axes
        plt.ylabel('Liquid Fraction',x = .1) 
        ax1.axvline(-0.03,color ='k')
        
        ax1.set_facecolor(self.Background_colour) # Setting colour of background
        ax1.patch.set_alpha(0.8)
        
        fig1.patch.set_facecolor('#02d8e9') # Colour of Border of window
        fig1.patch.set_alpha(0.7)
        
        plt.subplots_adjust(bottom=0.25) # Raises the bottom of the picture so that the slider can fit 
    
        self.first_temp[0,:] = self.phifactor*Array_1[0,:] # Plots the initial conditions
        im1, = ax1.plot(self.x,self.phifactor*Array_1[0,:],'b')
        if (self.input_lf == 0.0 and self.bubble_velocity == 0.0):
            '''Analytic Solution for Free Drainage: Steady State flow solution'''
            lamda2 = self.gamma/(self.water_density*self.gravity)
            z = np.arange(0, self.col_height, 0.001)
            phi = (mt.sqrt(3)/2.0)*(self.bubdiam/lamda2)*z
            phi += 1.0/mt.sqrt(self.base_lf)
            phi2 = 1.0/(phi**2)

            im3, = ax1.plot(z, phi2, '--r', label='Analytic Solution')
            plt.legend()

        self.im2 = ax1.pcolormesh(self.x,self.y,self.first_temp,norm=colors.Normalize(vmin=0., vmax=self.base_lf), cmap = self.cm)

        Glass =ax1.add_patch(mpatches.Rectangle((-.09,0),width=.035,height = 0.35,figure = fig1,edgecolor='w',fill = False, alpha=1)) #'lightgoldenrodyellow'
        Liquid = ax1.add_patch(mpatches.Rectangle((-.09,0),width=.035,height=(0.35/(self.Glass_Height))*self.Dimless_Liq_Array[0]*self.length_scale,figure = fig1,facecolor='k',alpha=1))
        Head =ax1.add_patch(mpatches.Rectangle((-.09,0),width=.035,height = (0.35/(self.Glass_Height))*self.Dimless_Foam_Array[0]*self.length_scale,figure = fig1,facecolor = '#fffe7a',alpha=1)) #facecolor='#fffe7a'
        axtime = plt.axes([.2, 0.1, 0.65, 0.03], facecolor='#d648d7') #Gives slider position values and slider colours
        self.stime = Slider(axtime, 'Time (s)', 0.0, self.dt*self.time_scale*self.animate_separation*(Array_1[:,0].size-1), valinit=0.0)
        def reset(event):
            self.stime.reset() # Function allows reset button to make everything zero again

        def update(val):
            time = int(self.stime.val*1/(self.dt*self.time_scale*self.animate_separation)) #Function chooses image number to display using the slider
            
            im1.set_xdata(self.x[:] )
            im1.set_ydata(self.phifactor*Array_1[time,:].ravel())
            
            self.im2.set_array(self.phifactor*Array_1[time,:].ravel())
            col = self.im2.get_facecolor()
            
            Liquid.set_height((0.35/(self.Glass_Height))*self.Dimless_Liq_Array[time]*self.phifactor*self.length_scale)
            Head.set_xy((-.09,(0.35/(self.Glass_Height))*self.Dimless_Liq_Array[time]*self.phifactor*self.length_scale))
            Head.set_height((0.35/(self.Glass_Height))*(1.- ((self.Dimless_Liq_Array[time]*self.phifactor)/self.col_height))*self.Dimless_Foam_Array[time]*self.length_scale)
  
            Head.set_facecolor(col[int(self.firstdim * 0.6)])
            # Accounts for the liquid draining out; otherwise artificially raising the height
            
            if (self.input_lf > 0.0 and self.initial_lf < 0.01):
                '''Analytic solution with varying times for a solitary wave: wetting of a dry foam'''  
                test = self.x - self.length_scale * self.dz
                tao = time / self.phifactor
                alpha_array = []
                x1 = 1. + (self.x / self.length_scale)
                x0 = self.firstdim * self.dz - 1.#self.z_height * self.dz  
                for i,x in enumerate(x1):
                    if ((x <  (x0 - (self.input_lf * tao)))):
                           alpha = 0.
                    else:
                        alpha = self.input_lf * pow(np.tanh(mt.sqrt(self.input_lf) * ((x - x0) + self.input_lf * tao)), 2)
                    alpha_array.append(alpha)
                im4, = ax1.plot(self.x, alpha_array, '--r', label = 'Analytic Solution')
            fig1.canvas.draw()
            if (self.input_lf > 0.0 and self.initial_lf < 0.01):
                im4.remove()
            
        self.stime.on_changed(update)

        resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
        self.button = Button(resetax, 'Reset', color='lightgoldenrodyellow', hovercolor='0.975')
        self.button.on_clicked(reset)
        
#------------------------------------------------------------------------------------------------------------
    
    '''
    The animation portion of the code begins here
    '''
    
    def set_up_canvas_2(self):
        self.fig2=plt.figure()
        
        self.ax2 = plt.axes(xlim=(-.1,self.x[-1]),ylim=(-0.01,.5))
        
        self.ax2.xaxis.set_major_locator(plt.NullLocator()) #This couple of lines makes partial x-ticks
        self.ax2.set_xticks(np.append(self.ax2.get_xticks(),np.linspace(0.,(self.firstdim)*self.length_scale*self.dz,5)))
        
        self.ax2.yaxis.set_major_locator(plt.NullLocator()) #This couple of lines makes partial y-ticks
        self.ax2.set_yticks(np.append(self.ax2.get_yticks(),np.linspace(0.,.36,7)))
        
        self.ax2.spines['left'].set_position('zero') #Puts the axis in the middle
        self.ax2.spines['right'].set_color(None)
        
        plt.xlabel('Height (m)',x = .7) # Labelling the axes
        plt.ylabel('Liquid Fraction',x = .1) 
        
        self.ax2.axvline(-0.03,color ='k')
        
        self.ax2.set_facecolor('#bdf8a3') # Colour of background
        self.ax2.patch.set_alpha(0.8)
        
        self.fig2.patch.set_facecolor('#02d8e9') # Colour of Border
        self.fig2.patch.set_alpha(0.7)
        
        self.time_template = 'Time = %.2f s' # Timer Value and position
        self.time_text = self.ax2.text(0.65,0.9,'',transform = self.ax2.transAxes)
        
        self.points_glass = [[-.065,0],[-0.045,0],[-0.04,self.Glass_Height],[-.07,self.Glass_Height]]
        self.points_liquid = [[-.065,0],[-0.045,0],[-.065,0],[-0.045,0]]

        self.Glass =self.ax2.add_patch(mpatches.Rectangle((-.09,0),width=.035,height = 0.35,figure = self.fig2,edgecolor='lightgoldenrodyellow',fill =False,alpha=1))
        self.Liquid = self.ax2.add_patch(mpatches.Rectangle((-.09,0),width=.035,height=(0.35/(self.Glass_Height))*self.Dimless_Liq_Array[0]*self.length_scale,figure = self.fig2,facecolor='k',alpha=1))
        self.Head =self.ax2.add_patch(mpatches.Rectangle((-.09,0),width=.035,height = (0.35/(self.Glass_Height))*self.Dimless_Foam_Array[0]*self.length_scale,figure = self.fig2,facecolor='#fffe7a',alpha=1))
#------------------------------------------------------------------------------------------------------------
    
    '''
    Relevant for the animation's attributes
    '''
    
    def Click_Pause(self,event):
        
        '''
        Pauses/Unpauses the Animation on a screen click
        '''
        
        if self.pause:
            for i in range(0,len(self.Animation_list)):
                self.Animation_list[i].event_source.stop()

        else:
            for i in range(0,len(self.Animation_list)):
                self.Animation_list[i].event_source.start()
        
        self.pause ^= True
        
    def Timer_funk(self,i):
    
        '''
        Gives the timer a time value
        '''
    
        self.time_text.set_text(self.time_template % (i*self.dt*self.time_scale*self.animate_separation))
        
        return self.time_text
    
    def Head_Funk(self,time):
         
        '''
        Makes the foam head which represents the foam
        '''
        
        H_VAl = self.Head.set_height((0.35/(self.Glass_Height))*(1.- ((self.Dimless_Liq_Array[time]*self.phifactor)/self.col_height))*self.Dimless_Foam_Array[time]*self.length_scale)
        
        return H_VAl
        
    def Head_Height_Funk(self,time):
        '''
        Makes the foam rise to correct location
        '''
        HH_VAL = self.Head.set_xy((-.09,(0.35/(self.Glass_Height))*self.Dimless_Liq_Array[time]*self.phifactor*self.length_scale))
        
        return HH_VAL
        
    def Liquid_Funk(self,time):
    
        '''
        Makes the black rectangle which represents the fluid
        '''
        
        L_Val = self.Liquid.set_height((0.35/(self.Glass_Height))*self.Dimless_Liq_Array[time]*self.phifactor*self.length_scale)
        
        return L_Val
    
#------------------------------------------------------------------------------------------------------------
        
    def GoAnimate(self,A_Array): 
        
        '''
        Function calls alpha array and appends array info to list in form of wrapped plot
        '''
        
        for i in range(0,1): 
            self.first_temp[i,:] = A_Array[:]
            
        self.Image_One.append((plt.pcolor(self.x,self.y,self.first_temp,norm=colors.Normalize(vmin=0., vmax=self.base_lf), cmap=self.cm),))
        
        self.second_temp[self.Imagecounter,:] = A_Array[:]
        
        self.Image_Two.append((plt.plot(self.x,self.second_temp[self.Imagecounter,:],'r')),)    
        
    def For_Loop_That_Makes_Animation(self):
        for i in range(0,Array_1[:,0].size):
            self.GoAnimate(self.phifactor*Array_1[i,:])
            self.Imagecounter += 1
            
    def Return_Animation(self):
        self.Animation_list = [
            animation.ArtistAnimation(self.fig2, self.Image_One, interval=self.interval, repeat_delay=self.delay, blit=False), #Arguments change how the animation is shown
            animation.ArtistAnimation(self.fig2, self.Image_Two, interval=self.interval, repeat_delay=self.delay, blit=False),
            animation.FuncAnimation(self.fig2,self.Timer_funk, np.arange(1,len(self.Image_One)),interval=self.interval, repeat_delay=self.delay, blit=False),
            animation.FuncAnimation(self.fig2,self.Head_Funk, np.arange(1,len(self.Image_One)),interval=self.interval, repeat_delay=self.delay, blit=False),
            animation.FuncAnimation(self.fig2,self.Head_Height_Funk, np.arange(1,len(self.Image_One)),interval=self.interval, repeat_delay=self.delay, blit=False),
            animation.FuncAnimation(self.fig2,self.Liquid_Funk, np.arange(1,len(self.Image_One)),interval=self.interval, repeat_delay=self.delay, blit=False)
        ]
        
        self.fig2.canvas.mpl_connect('button_press_event', self.Click_Pause)
        
#------------------------------------------------------------------------------------------------------------

Slider_y_n = input('Would you like a slider-style animation of the Column? (y for yes, n for no):')
Animated_y_n = input('Would you like an animation of the Column? (y for yes, n for no):')

start_time = datetime.now()

if Slider_y_n == 'y' or Slider_y_n == 'yes' or Slider_y_n == 'Y' or Slider_y_n == '1':
    Slider_y_n = True
else:
    Slider_y_n = False
    
if Animated_y_n == 'y' or Animated_y_n == 'yes' or Animated_y_n == 'Y' or Animated_y_n == '1':
    Animated_y_n = True
else:
    Animated_y_n = False

print('Working on it boss...')
Instance = AnimationClass(Slider_y_n,Animated_y_n)
