import numpy as np
import math as mt 
import sys, libconf, io
from datetime import datetime
from copy import deepcopy

with io.open('config.cfg') as Conf_file: # Config file is read in
    Con = libconf.load(Conf_file)

#------------------------------------------------------------------------------------------------------------

class Column:
    '''
    Class should construct a column of foam
    '''
    
    def __init__(self):
        
        '''
        Constructor Initialises the most changed vars
        and sets up the 'alpha array' 
        Important to note that crit lf min is used as the crit liquid fraction in this program
        '''
        
        self.Physical_Quants()
        self.Other_Variables()
        self.Alpha_Setup()

        phifactor_inverse = 1.0/self.phifactor # Avoid multiple floating point divisions
        self.input_lf = Con.input_lf*phifactor_inverse
        self.initial_lf = Con.initial_lf*phifactor_inverse
        self.base_lf = Con.base_lf*phifactor_inverse
        self.crit_lf = Con.crit_lf_min*phifactor_inverse # liquid fraction below which bubbles rupture
        self.Initialise_Alpha_Array()
        self.Integrate()
        
#------------------------------------------------------------------------------------------------------------

    '''
    Function relating to the rate of liquid input, x is time, enter a function of time pref. with range [0,1]
    ''' 
    def Input_Function(self,x):
        return 1 

#------------------------------------------------------------------------------------------------------------
    
    '''
    Step Sizes for integration scheme
    '''
    def Time_Steps(self):
        self.dt = Con.dt 
        self.dz = Con.dz
        self.dz1 = 1.0/self.dz 
        self.rz1 = self.dt*self.dz1 #d(tau)/d(xi)

#------------------------------------------------------------------------------------------------------------
    
    '''
    Physical Constants
    '''
    def Physical_Quants(self):
        self.gamma = Con.gamma
        self.C_factor = Con.C_factor
        self.f_factor = Con.f_factor
        self.gravity = Con.gravity
        self.water_density = Con.water_density
        self.water_viscosity = Con.water_viscosity 
        self.bubdiam = Con.bubdiam #m
        self.col_height = Con.col_height
        self.base_liquid = 0.
        
#------------------------------------------------------------------------------------------------------------
    
    '''
    Variables generated from other values and extras 
    '''
    def Other_Variables(self):
        self.bubble_velocity = Con.bubble_vel
        self.runtime = Con.Dimless_runtime
        self.plot_separation = Con.plot_separation
        self.animate_separation = Con.animate_separation
        self.Time_Steps()
        self.bubvol = mt.pi*self.bubdiam**3/6
        self.effective_viscosity = 3.*self.f_factor*self.water_viscosity
        self.length_scale = mt.sqrt((self.C_factor*self.gamma)/(self.water_density*self.gravity))
        self.time_scale = self.effective_viscosity*(1./mt.sqrt(self.C_factor*self.gamma*self.water_density*self.gravity))
        self.phifactor = (5.35*self.length_scale*self.length_scale)/pow( ((4./3.)*mt.pi*(self.bubdiam * 0.5)**3), 2./3. )
        self.dimless_bubble_velocity = self.bubble_velocity*self.time_scale/self.length_scale
        self.maxtime = int(self.runtime/self.dt)
        
        print("Dimensional Runtime: ", self.runtime * self.time_scale,"s")
        
        self.plot_times = self.dt * np.arange(0., self.maxtime + 1, self.plot_separation)
        self.animate_times = self.dt * np.arange(0., self.maxtime + 1, self.animate_separation)
        self.jp = 0
        self.ja = 0        
        self.test = (np.linspace(0,self.maxtime,100)).astype(int)
        
#------------------------------------------------------------------------------------------------------------
    
    '''
    Variables relating to the 'alpha' array
    '''
    def Alpha_Setup(self):
        self.z_height = int(self.col_height/(self.length_scale*self.dz))
        self.firstdim = (self.z_height + 1)
        self.seconddim = 2
        self.alpha = np.zeros((self.firstdim, self.seconddim)) #matrix dim = fractional height x 2
        self.q = np.zeros(self.alpha[:,0].size)
        
        self.Saving_Alpha_List = np.zeros([int(self.maxtime/self.animate_separation)+1, self.firstdim])
        self.Plot_Alpha_Array = np.zeros([self.firstdim, int(self.maxtime/self.plot_separation)+1])
        self.Plot_Height_Array = np.zeros([int(self.maxtime/self.plot_separation)+1])
        self.Plot_Height_Increase_Array = np.zeros(int(self.maxtime/self.animate_separation)+1)
        
#------------------------------------------------------------------------------------------------------------

    def Initialise_Alpha_Array(self):
        '''
        Initialise the Alpha Array
        '''
        self.alpha[:2,:] = self.base_lf
        self.alpha[2:-1,:] = self.initial_lf
    
#------------------------------------------------------------------------------------------------------------    
    
    def _Ni_Pi_(self, a):
        '''
        pi and ni alternate between columns of alpha (previous and next iterations)
        '''
        self.pi = (a + 1) % 2  
        self.ni = a % 2  #used to cycle between the two dimensions of alpha
        
    def Find_Height_Index(self):
        '''
        Find height of foam at each instance in time
        '''
        self.temp = deepcopy(self.alpha[:-1, self.pi]) #temporary storage array for new alpha values to check and edit
        self.h_index = len(self.temp[self.temp >= self.crit_lf]) #finds the highest entry in temp with nonzero liquid fraction (liq fraction is equal to critical value)
    
    def Rupture(self):
        '''
        Rupture of Bubbles scheme goes in here
        '''
        self.alpha[self.h_index:, self.pi] = 0.       
    
    def Find_Dimless_Base_Height(self):
    
        self.base_liquid += self.q[0] * self.dt
        if self.base_liquid <= 0.:
            self.dimless_base_height = 0.
            self.base_liquid = 0.
        if self.base_liquid > 0.:
            self.dimless_base_height = self.base_liquid #liquid fraction is one at this point
            
    def Animation_Arrays_Append(self, Array, i):
        self.Saving_Alpha_List[i,:] = Array[:]
        self.Plot_Height_Increase_Array[i] = self.dimless_base_height
        
    def Plot_Arrays_Update(self):
        self.Plot_Alpha_Array[:, self.jp] = self.alpha[:, self.pi]
        self.Plot_Height_Array[self.jp] = (self.h_index - 1 ) * self.dz
    
    def Integrate_Step_Down(self,Time): #Integration Scheme for Free and Forced Drainage
        self.q[:self.h_index-1] = self.alpha[1:self.h_index, self.pi] **2 + np.sqrt(self.alpha[1:self.h_index, self.pi]) * 0.5 * np.ediff1d(self.alpha[:self.h_index, self.pi]) * self.dz1
        self.q[self.h_index-1] = (self.input_lf*self.Input_Function(Time))**2 
        self.q[self.h_index:] = 0.
        
        self.alpha[1:, self.ni] = self.alpha[1:, self.pi] + self.rz1 * (np.ediff1d(self.q))    

    def Integrate_Step_Up(self): #Integration Scheme for Gas Inflow at Bottom
        self.q[0] = - self.base_lf * self.dimless_bubble_velocity + self.alpha[0, self.pi] * self.alpha[0, self.pi] + np.sqrt(self.alpha[0, self.pi]) * 0.5 * np.ediff1d(self.alpha[0:2, self.pi]) * self.dz1
        self.q[1:self.h_index-1] = - self.alpha[0:self.h_index-2, self.pi] * self.dimless_bubble_velocity + self.alpha[1:self.h_index-1, self.pi] * self.alpha[1:self.h_index-1, self.pi] + np.sqrt(self.alpha[1:self.h_index-1, self.pi]) * 0.5 * np.ediff1d(self.alpha[1:self.h_index, self.pi]) * self.dz1
        self.q[self.h_index-1] = self.input_lf * self.input_lf - self.alpha[self.h_index-2, self.pi] * self.dimless_bubble_velocity + self.alpha[self.h_index-1, self.pi] * self.alpha[self.h_index-1, self.pi] + np.sqrt(self.alpha[self.h_index-1, self.pi]) * 0.5 * np.ediff1d(self.alpha[self.h_index-1: self.h_index+1, self.pi]) * self.dz1
        self.q[self.h_index:] = 0. 
        
        self.alpha[1:, self.ni] = self.alpha[1:, self.pi] + self.rz1 * np.ediff1d(self.q)
#------------------------------------------------------------------------------------------------------------

    def Integrate(self):
        
        '''
        Calls Integration scheme in loop and functions which save the array for later use
        '''
        
        for i in range (0, self.maxtime+1): #time-evolve the system
            
            if i in self.test: # Says what percent of the way through the simulation it is at
                print("{0} %".format(100*i/self.maxtime), flush = True) 
                
            self._Ni_Pi_(i)
            self.Find_Height_Index()
            self.Find_Dimless_Base_Height()
            if self.bubble_velocity == 0.:
                self.Rupture()
            
            if i*self.dt in self.plot_times:
                self.Plot_Arrays_Update()
                self.jp += 1
            
            if self.input_lf != 0.:
                if self.bubble_velocity == 0.:
                    self.Integrate_Step_Down(i*self.dt)
                else:
                    print('Sorry, that set-up is currently invalid.')
                    sys.exit()
            else:
                if self.bubble_velocity != 0.:
                    self.Integrate_Step_Up()

                if self.bubble_velocity == 0.:
                    self.Integrate_Step_Down(i*self.dt)
                
            if i*self.dt in self.animate_times:
                self.Animation_Arrays_Append(self.alpha[:, self.ni], self.ja)
                self.ja += 1

#------------------------------------------------------------------------------------------------------------
                
    def Save_Arrays(self, Alphas, Heights, Animation,Height_Increase):
        np.save('Plot_Profiles_Array.npy', Alphas)
        np.save('Plot_Heights_Array.npy', Heights)
        np.save('Beer_Animation_Array.npy', Animation)
        np.save('Beer_Increase_Height.npy',Height_Increase)
        
#------------------------------------------------------------------------------------------------------------
        
print('Working on it boss...')

start_time = datetime.now()
Instance = Column()
Instance.Save_Arrays(Instance.Plot_Alpha_Array, Instance.Plot_Height_Array, Instance.Saving_Alpha_List,Instance.Plot_Height_Increase_Array)

end_time = datetime.now()
print('Running Duration: {}'.format(end_time - start_time))
