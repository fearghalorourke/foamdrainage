"""
Program made in order to fit the height vs. time graph
"""

import numpy as np
import math as mt
import scipy.optimize as sco
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import libconf, io

with io.open('config.cfg') as Conf_file:
        Con = libconf.load(Conf_file)

#--------------------------------------------------------------------------------------------------

class Fit_Class:

    def __init__(self):
        
        self.Steps()
        self.Physical_Parameters()
        self.Inputted_Parameters()
        self.Scaling_Parameters()
        self.Read_In_Array()
        self.Canvas_Create()
        
#--------------------------------------------------------------------------------------------------

    def Steps(self):
    
        self.dt = Con.dt
        self.dz = Con.dz
        
    Fit_Tanh = lambda self, x, a, b, c, d: a*(np.tanh(b*(x + c)) + d)
    
    def Physical_Parameters(self):
    
        self.gamma = Con.gamma
        self.C_factor = Con.C_factor
        self.f_factor = Con.f_factor
        self.gravity = Con.gravity
        self.water_viscosity = Con.water_viscosity
        self.water_density = Con.water_density
        self.bubdiam = Con.bubdiam
        
        self.bubvol = mt.pi*self.bubdiam*self.bubdiam*self.bubdiam/6
        self.effective_viscosity = 3.*self.f_factor*self.water_viscosity
        self.pg = self.water_density*self.gravity
        self.fn = self.f_factor*self.water_viscosity
        self.vol_factor = mt.pow(mt.pi*mt.pow(self.bubdiam, 3)/6, 2./3.)/5.35
        
    def Inputted_Parameters(self):
        
        self.runtime = Con.Dimless_runtime
        self.maxtime = int(self.runtime / self.dt)
        
    def Scaling_Parameters(self):
    
        self.length_scale = mt.sqrt((self.C_factor*self.gamma)/(self.water_density*self.gravity))
        self.time_scale = self.effective_viscosity*(1./mt.sqrt(self.C_factor*self.gamma*self.water_density*self.gravity))
        self.phifactor = (5.35*self.length_scale*self.length_scale)/pow( ((4./3.)*mt.pi*(self.bubdiam * 0.5)*(self.bubdiam * 0.5)*(self.bubdiam * 0.5)), 2./3. )
    
#--------------------------------------------------------------------------------------------------
        
    def Read_In_Array(self):
        
        self.Height_Array = np.load('Plot_Heights_Array.npy')
        self.Number_Of_Time_Steps = self.Height_Array.size
        self.Highest_Point = max(self.Height_Array)
        self.plot_times = self.dt * np.linspace(0., self.maxtime, self.Number_Of_Time_Steps)

#--------------------------------------------------------------------------------------------------    
        
    def Canvas_Create(self):
        
        fig = plt.figure()
        ax = plt.axes(xlim = (self.plot_times[0],self.plot_times[-1]),ylim = (0.,self.Highest_Point*1.4))
        plt.subplots_adjust(bottom=0.3)
        
        self.a = 1.
        self.b = 1e-6
        self.c = 6e6
        self.d = 1.
        
        plt.plot(self.plot_times,self.Height_Array,'r',figure = fig)
        self.Plotted, = plt.plot(self.plot_times,self.a*np.tanh(self.b*(self.plot_times +self.c))+self.d,'--k')
        
        axtime_a = plt.axes([.05,.2, 0.65, 0.03]) 
        axtime_b = plt.axes([.05,.15, 0.65, 0.03])
        axtime_c = plt.axes([.05,.1,0.65,.03])
        axtime_d = plt.axes([.05,.05,0.65,.03])
        
        self.a_Slider = Slider(axtime_a, 'a', -1.1*self.Highest_Point, 1.1*self.Highest_Point, valinit=0.0)
        self.b_Slider = Slider(axtime_b, 'b', -1./(self.plot_times[-1]*self.dt), 1./(self.plot_times[-1]*self.dt), valinit=0.0, valfmt = '%f')
        self.c_Slider = Slider(axtime_c, 'c', -self.plot_times[-1], self.plot_times[-1], valinit=0.0)
        self.d_Slider = Slider(axtime_d, 'd', -1000, 1000, valinit=0.0)

    
        def update_a(val):
            self.a = int(self.a_Slider.val) #Function chooses image number to display using the slider
            
            self.Plotted.set_ydata(self.a*np.tanh(self.b*(self.plot_times +self.c))+self.d)
            fig.canvas.draw()
    
        def update_b(val):
            self.b = (self.b_Slider.val)
            
            self.Plotted.set_ydata(self.a*np.tanh(self.b*(self.plot_times +self.c))+self.d)
            fig.canvas.draw()
    
        def update_c(val):
            self.c = int(self.c_Slider.val)
            
            self.Plotted.set_ydata(self.a*np.tanh(self.b*(self.plot_times +self.c))+self.d)
            fig.canvas.draw()
        
        def update_d(val):
            self.d = -int(self.d_Slider.val)
            
            self.Plotted.set_ydata(self.a*np.tanh(self.b*(self.plot_times +self.c))+self.d)
            fig.canvas.draw()
            
        self.a_Slider.on_changed(update_a)
        self.b_Slider.on_changed(update_b)
        self.c_Slider.on_changed(update_c)
        self.d_Slider.on_changed(update_d)
        
#--------------------------------------------------------------------------------------------------        

Fitting_Name = Fit_Class()
plt.show()

(a, b, c, d), pcov_T = sco.curve_fit(Fitting_Name.Fit_Tanh,Fitting_Name.plot_times,Fitting_Name.Height_Array,[Fitting_Name.a, Fitting_Name.b, Fitting_Name.c,Fitting_Name.d])

fig2 = plt.figure()

ax = plt.axes(xlim = (Fitting_Name.plot_times[0],Fitting_Name.plot_times[-1]),ylim = (0.,Fitting_Name.Highest_Point*1.4))
plt.plot(Fitting_Name.plot_times,Fitting_Name.Fit_Tanh(Fitting_Name.plot_times,a,b,c,d),'--k',figure = fig2)
plt.plot(Fitting_Name.plot_times,Fitting_Name.Height_Array,'r',figure = fig2)

print('{0}'.format((a,b,c,d)))

plt.show()
