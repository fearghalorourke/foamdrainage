import tkinter, os
from tkinter import Tk
from tkinter import *

top = Tk()  #Initial window is 'top'

top.wm_title("Foam Simulator  ( Remember to hit Submit when done!!! ) ")  #Gives title to parent window

background = '#02d8e9'
top.configure(background = background)

#------------------------------------------------------------------------------------------------------------

def config_maker(): 
    
    '''
    This function makes the GUI window and presets the values
    '''
    
    Dimless_runtime_text = StringVar()
    Dimless_runtime_text.set("Duration to be simulated (Dimensionlessly): ")
    Dimless_runtime_label = Label(top,bg = 'red', textvariable = Dimless_runtime_text).grid(row = 0, column = 1, sticky = 'W')
    Dimless_runtime = Entry(top)
    Dimless_runtime.insert(0,1000)
    Dimless_runtime.grid(row = 0, column = 2, sticky = 'W')
    
    dimless_plots_text = StringVar()
    dimless_plots_text.set("Would you like dimensionless plots? (1 for 'yes'; 0 for 'no'): ")
    dimless_plots_label = Label(top,bg = background, textvariable = dimless_plots_text).grid(row = 1, column = 0, sticky = 'W')
    dimless_plots = Entry(top)
    dimless_plots.insert(0,1)
    dimless_plots.grid(row = 1, column = 1, sticky = 'W')
    
    dimal_plots_text = StringVar()
    dimal_plots_text.set("Would you like dimensional plots? (1 for 'yes'; 0 for 'no'): ")
    dimal_plots_label = Label(top,bg = background, textvariable = dimal_plots_text).grid(row = 1, column = 2, sticky = 'W')
    dimal_plots = Entry(top)
    dimal_plots.insert(0,1)
    dimal_plots.grid(row = 1, column = 3, sticky = 'W')
    
    plot_separation_text = StringVar()
    plot_separation_text.set("Number of time steps between plotted profiles: ")
    plot_separation_label = Label(top,bg = background, textvariable = plot_separation_text).grid(row = 2, column = 0, sticky = 'W')
    plot_separation = Entry(top)
    plot_separation.insert(0,100)
    plot_separation.grid(row = 2, column = 1, sticky = 'W')
    
    animate_separation_text = StringVar()
    animate_separation_text.set("Number of time steps between animation frames: ")
    animate_separation_label = Label(top,bg = background, textvariable = animate_separation_text).grid(row = 2, column = 2, sticky = 'W')
    animate_separation = Entry(top)
    animate_separation.insert(0,100)
    animate_separation.grid(row = 2, column = 3, sticky = 'W')
    
    col_height_text = StringVar()
    col_height_text.set("Height of glass column (m): ")
    col_height_label = Label(top,bg = background, textvariable = col_height_text).grid(row = 3, column = 0, sticky = 'W')
    col_height = Entry(top)
    col_height.insert(0,.2)
    col_height.grid(row = 3, column = 1, sticky = 'W')
    
    bubble_vel_text = StringVar() 
    bubble_vel_text.set("Bubble velocity (m/s): ")
    bubble_vel_label = Label(top,bg = 'red', textvariable = bubble_vel_text).grid(row = 3, column = 2, sticky = 'W')
    bubble_vel = Entry(top)
    bubble_vel.insert(0,0)
    bubble_vel.grid(row = 3, column = 3, sticky = 'W')
    
    input_lf_text = StringVar()
    input_lf_text.set("Liquid fraction magnitude for liquid input at the top: ")
    input_lf_label = Label(top,bg = 'red', textvariable = input_lf_text).grid(row = 4, column = 0, sticky = 'W')
    input_lf = Entry(top)
    input_lf.insert(0,0)
    input_lf.grid(row = 4, column = 1, sticky = 'W')
    
    initial_lf_text = StringVar()
    initial_lf_text.set("Initial uniform liquid fraction: ")
    initial_lf_label = Label(top,bg = 'red', textvariable = initial_lf_text).grid(row = 4, column = 2, sticky = 'W')
    initial_lf = Entry(top)
    initial_lf.insert(0,0)
    initial_lf.grid(row = 4, column = 3, sticky = 'W')
    
    gamma_text = StringVar()
    gamma_text.set("Surface tension of liquid (N/m): ")
    gamma_label = Label(top,bg = background, textvariable = gamma_text).grid(row = 5, column = 0, sticky = 'W')
    gamma = Entry(top)
    gamma.insert(0,72e-3)
    gamma.grid(row = 5, column = 1, sticky = 'W')
    
    gravity_text = StringVar()
    gravity_text.set("Acceleration due to gravity (m/s^2): ")
    gravity_label = Label(top,bg = background, textvariable = gravity_text).grid(row = 5, column = 2, sticky = 'W')
    gravity = Entry(top)
    gravity.insert(0,9.81)
    gravity.grid(row = 5, column = 3, sticky = 'W')
    
    water_density_text = StringVar()
    water_density_text.set("Density of liquid (kg/m^3): ")
    water_density_label = Label(top,bg = background, textvariable = water_density_text).grid(row = 6, column = 0, sticky = 'W')
    water_density = Entry(top)
    water_density.insert(0,1000)
    water_density.grid(row = 6, column = 1, sticky = 'W')
    
    water_viscosity_text = StringVar()
    water_viscosity_text.set("Viscosity of liquid (Pl s): ")
    water_viscosity_label = Label(top,bg = background, textvariable = water_viscosity_text).grid(row = 6, column = 2, sticky = 'W')
    water_viscosity = Entry(top)
    water_viscosity.insert(0,8.9e-4)
    water_viscosity.grid(row = 6, column = 3, sticky = 'W')
    
    bubdiam_text = StringVar()
    bubdiam_text.set("Bubble diameter (m): ")
    bubdiam_label = Label(top,bg = background, textvariable = bubdiam_text).grid(row = 7, column = 0, sticky = 'W')
    bubdiam = Entry(top)
    bubdiam.insert(0,7e-3)
    bubdiam.grid(row = 7, column = 1, sticky = 'W')
    
    C_factor_text = StringVar()
    C_factor_text.set("C factor: ")
    C_factor_label = Label(top,bg = background, textvariable = C_factor_text).grid(row = 7, column = 2, sticky = 'W')
    C_factor = Entry(top)
    C_factor.insert(0,0.40156504)
    C_factor.grid(row = 7, column = 3)
    
    f_factor_text = StringVar()
    f_factor_text.set("f factor (effective viscosity = 3f * viscosity): ")
    f_factor_label = Label(top,bg = background, textvariable = f_factor_text).grid(row = 8, column = 0, sticky = 'W')
    f_factor = Entry(top)
    f_factor.insert(0,49.)
    f_factor.grid(row = 8, column = 1, sticky = 'W')
    
    base_lf_text = StringVar()
    base_lf_text.set("Liquid fraction at foam-liquid interface: ")
    base_lf_label = Label(top,bg = background, textvariable = base_lf_text).grid(row = 8, column = 2, sticky = 'W')
    base_lf = Entry(top)
    base_lf.insert(0,.36)
    base_lf.grid(row = 8, column = 3, sticky = 'W')
    
    crit_lf_max_text = StringVar()
    crit_lf_max_text.set("Max critical liquid fraction below which bubble rupture occurs: ")
    crit_lf_max_label = Label(top,bg = 'red', textvariable = crit_lf_max_text).grid(row = 9, column = 2, sticky = 'W')
    crit_lf_max = Entry(top)
    crit_lf_max.insert(0,.01)
    crit_lf_max.grid(row = 9, column =3, sticky = 'W')

    crit_lf_min_text = StringVar()
    crit_lf_min_text.set("Min critical liquid fraction below which bubble rupture occurs: ")
    crit_lf_min_label = Label(top,bg = 'red', textvariable = crit_lf_min_text).grid(row = 9, column = 0, sticky = 'W')
    crit_lf_min = Entry(top)
    crit_lf_min.insert(0,0.0001)
    crit_lf_min.grid(row = 9, column =1, sticky = 'W')
        
    blank_space = Label(top,bg = background).grid(row = 11, column = 0, columnspan = 4)
    
    # Submit button must be pressed to make changes to the file
    
    submit_button = Button(top, bg = 'yellow', text="Submit Configuration Settings", command = lambda: submit_config( Dimless_runtime,
    dimless_plots, dimal_plots, plot_separation, animate_separation, bubble_vel, input_lf, initial_lf, gamma, gravity, 
    water_density, water_viscosity, bubdiam, col_height, C_factor, f_factor, crit_lf_max, crit_lf_min, base_lf)).grid(row = 10, column = 0, columnspan = 1)
    
    Run_Const_button = Button(top, bg = 'green', text = "Run a Simulation Using Const Crit lf", command = lambda: os.system("python3 DimlesFDEConstCritLf.py")).grid(row= 10,column = 1,columnspan = 1)
    
    Run_var_button = Button(top, bg = 'green', text = "Run a Simulation Using Variable Crit lf", command = lambda: os.system("python3 DimlesFDEVarCritLf.py")).grid(row= 10,column = 2,columnspan = 1)

    Animate_button = Button(top, bg = 'blue', text = "Animate the Simulation", command = lambda: os.system("python3 Animation.py")).grid(row= 10,column = 3,columnspan = 1)
#------------------------------------------------------------------------------------------------------------

def submit_config(Dimless_runtime, dimless_plots, dimal_plots, plot_separation, animate_separation, 
    bubble_vel, input_lf, initial_lf, gamma, gravity, water_density, water_viscosity, bubdiam, 
    col_height, C_factor, f_factor, crit_lf_max, crit_lf_min, base_lf):
    
    '''
    Function writes to the config.cfg file, after opening
    Called in other function
    '''

    file = open("config.cfg", "w") # File opened
    
    file.write("//This is the config.cfg file.\n")
    file.write("//The parameters contained inside this file govern what the simulation, plots, animation etc\n")
    file.write("//can see and use. \n\n")
    
    file.write("Dimless_runtime = %f;// \n\n" % float(Dimless_runtime.get())) # Previously inputted values are written to file
    file.write("dimensionless_plots = %d;// \n" % int(dimless_plots.get()))
    file.write("dimensional_plots = %d;// \n\n" % int(dimal_plots.get()))
    file.write("plot_separation = %d;// \n" % int(plot_separation.get()))
    file.write("animate_separation = %d;// \n\n" % int(animate_separation.get()))
    file.write("bubble_vel = %f;//(m/s) \n" % float(bubble_vel.get()))
    file.write("input_lf = %f;// \n" % float(input_lf.get()))
    file.write("initial_lf = %f;// \n\n" % float(initial_lf.get()))
    file.write("gamma = %f;//(N/m) \n" % float(gamma.get()))
    file.write("gravity = %f;//(m/s^2) \n" % float(gravity.get()))
    file.write("water_density = %f;//(kg/m^3) \n" % float(water_density.get()))
    file.write("water_viscosity = %f;//(Pl s) \n" % float(water_viscosity.get()))
    file.write("bubdiam = %f;//(m) \n" % float(bubdiam.get()))
    file.write("col_height = %f;//(m) \n\n" % float(col_height.get()))
    file.write("C_factor = %f;// \n" % float(C_factor.get()))
    file.write("f_factor = %f;// \n\n" % float(f_factor.get()))
    file.write("crit_lf_max = %f;// \n" % float(crit_lf_max.get()))
    file.write("crit_lf_min = %f// \n" % float(crit_lf_min.get()))
    file.write("base_lf = %f;// \n\n" % float(base_lf.get()))
    file.write("Glass_Height = %f;//(m) \n\n" % float(col_height.get()))
    
    file.write("dt = %f;// \n" % .01) # These affect stability of simulation's integration scheme
    file.write("dz = %f;// \n" % .1) #  and are therefore not accessible by user

    file.close() 

#------------------------------------------------------------------------------------------------------------

config_maker() 

top.mainloop()
