import numpy as np
import math as mt 
import libconf, io
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from datetime import datetime


with io.open('config.cfg') as Conf_file:
    Con = libconf.load(Conf_file)

print("Working on it boss...")

#------------------------------------------------------------------------------------------------------------

class PlotClass:
    
    def __init__(self):
    
        '''
        Ensures that the correct plots are chosen
        based on the conditions specified in the config file
        '''
        
        self.set_what_to_plot()
        self.Input_Parameters()

        self.Int_Steps()
        self.Import_Arrays()
        
        self.Bubbling_Crit_Vel()
        
        self.Theoretical_SSH() #Template for steady state height calculation

        if self.input_lf == 0.:
            self.Free_Drainage_SS_Height_min()
            self.Free_Drainage_SS_Height_max()
            self.Free_Drainage_SS_Prof()
        self.Critical_Alpha_Line()
        if self.bubble_velocity > 0.:
            if self.dimless_bubble_velocity < self.crit_lf_min:
                self.Bubbling_SS_Height()
            self.Bubbling_SS_Prof()
        if self.dimensionless_plots:
            self.Plot_Dimensionless()
        if self.dimensional_plots:
            self.Plot_Dimensional()

#------------------------------------------------------------------------------------------------------------
            
    def set_what_to_plot(self):
        
        '''
        Says which plots are wanted via Booleans from the config file
        '''
        self.dimensionless_plots = Con.dimensionless_plots
        self.dimensional_plots = Con.dimensional_plots
        
    def Physical_Quants(self):
    
        '''
        Physical parameters as specified in config.cfg
        '''
        
        self.gamma = Con.gamma
        self.C_factor = Con.C_factor
        self.f_factor = Con.f_factor
        self.gravity = Con.gravity
        self.water_viscosity = Con.water_viscosity
        self.water_density = Con.water_density
        self.bubdiam = Con.bubdiam
        
        self.bubvol = mt.pi*self.bubdiam*self.bubdiam*self.bubdiam/6
        self.effective_viscosity = 3.*self.f_factor*self.water_viscosity
        self.pg = self.water_density*self.gravity
        self.fn = self.f_factor*self.water_viscosity
        self.vol_factor = mt.pow(mt.pi*mt.pow(self.bubdiam, 3)/6, 2./3.)/5.35
     
    def Scaling(self):
    
        '''
        Scaling for dimensionful plots
        '''
        
        self.length_scale = mt.sqrt((self.C_factor*self.gamma)/(self.water_density*self.gravity))
        self.time_scale = self.effective_viscosity*(1./mt.sqrt(self.C_factor*self.gamma*self.water_density*self.gravity))
        self.phifactor = (5.35*self.length_scale*self.length_scale)/pow( ((4./3.)*mt.pi*(self.bubdiam * 0.5)*(self.bubdiam * 0.5)*(self.bubdiam * 0.5)), 2./3. )

    def Input_Parameters(self):
    
        '''
        Input parameters and variables
        '''
        
        self.bubble_velocity = Con.bubble_vel
        self.runtime = Con.Dimless_runtime
        self.prof_freq = Con.plot_separation
        self.Physical_Quants()
        self.Scaling()

        phifactor_inverse = 1.0/self.phifactor # Prevent multiple floating point divisions
        self.input_lf = Con.input_lf*phifactor_inverse
        self.initial_lf = Con.initial_lf*phifactor_inverse
        self.base_lf = Con.base_lf*phifactor_inverse
        self.crit_lf_min = Con.crit_lf_min*phifactor_inverse
        self.crit_lf_max = Con.crit_lf_max*phifactor_inverse
        self.dimless_bubble_velocity = self.bubble_velocity * self.time_scale / self.length_scale
        self.Total_Liquid_In_Foam = (Con.col_height/self.length_scale)*(self.initial_lf)
        self.Equilibrium_Avg_lf = (((2./np.sqrt(3.))*(self.gamma/(self.water_density*self.gravity))*(np.sqrt(self.base_lf)))/(self.bubdiam*Con.col_height))*(1. - 1./(1.+(np.sqrt(3.)/2.)*(self.bubdiam/(self.gamma/(self.water_density*self.gravity)))*Con.col_height*np.sqrt(self.base_lf)))
    
    def Int_Steps(self):
        self.dt = Con.dt 
        self.dz = Con.dz 
        
#------------------------------------------------------------------------------------------------------------
        
    def Import_Arrays(self):
        
        '''
        Imports the various arrays
        Gives the relevant heights
        '''
        
        self.profiles = np.load('Plot_Profiles_Array.npy')
        self.heights = np.load('Plot_Heights_Array.npy')
        self.height_increase = np.load('Beer_Increase_Height.npy')
        self.num_time_samples = self.heights.size
        self.num_time_samples2 = self.height_increase.size
        self.num_space_samples = self.profiles[:, 0].size
        self.col_height = self.num_space_samples * self.dz
        self.maxtime = int(self.runtime / self.dt)
        self.plot_times = self.dt * np.linspace(0., self.maxtime, self.num_time_samples)
        self.plot_times2 = self.dt * np.linspace(0., self.maxtime, self.num_time_samples2)
        self.plot_cells = np.arange(0., self.col_height, self.dz)
        self.highest_point = max(self.heights)

    def Bubbling_Crit_Vel(self):
        self.dimless_crit_velocity = self.crit_lf_min
        self.dimensional_crit_velocity = (self.length_scale / self.time_scale) * self.crit_lf_min

#------------------------------------------------------------------------------------------------------------
        
    def Free_Drainage_SS_Height_min(self):
        self.free_drainage_eq_height_min = 2*self.gamma/(np.sqrt(3)*self.bubdiam*self.pg)*(1/np.sqrt(self.phifactor * self.crit_lf_min) - 1/np.sqrt(self.phifactor * self.base_lf))
        self.dimless_free_drainage_eq_height_min = self.free_drainage_eq_height_min / self.length_scale

    def Free_Drainage_SS_Height_max(self):
        self.free_drainage_eq_height_max = 2*self.gamma/(np.sqrt(3)*self.bubdiam*self.pg)*(1/np.sqrt(self.phifactor * self.crit_lf_max) - 1/np.sqrt(self.phifactor * self.base_lf))
        self.dimless_free_drainage_eq_height_max = self.free_drainage_eq_height_max / self.length_scale
        
    def Free_Drainage_SS_Prof(self): # free drainage equilibrium profile
        self.free_eq_prof = np.power(1/(mt.sqrt(self.base_lf * self.phifactor)) + (mt.sqrt(3) * self.bubdiam * self.length_scale * np.linspace(0.0, self.col_height, self.num_space_samples))/(2 * (self.gamma/(self.water_density * self.gravity))), -2) #Eqns 10.7 - 10.9 H&W 
        self.dimless_free_eq_prof = self.free_eq_prof/self.phifactor

    def Critical_Alpha_Line(self):
        self.crit_lf_line_lf_min = np.array([self.crit_lf_min, self.crit_lf_min])
        self.crit_lf_line_lf_max = np.array([self.crit_lf_max, self.crit_lf_max])
        self.crit_lf_line_z = np.array([0., self.col_height])

        
    def Bubbling_SS_Height(self):
        
        '''The expected dependency of the Steady State height on the inputted velocity'''

        numerator = np.sqrt(self.crit_lf_min * self.base_lf) - self.dimless_bubble_velocity
        denominator = np.sqrt(self.dimless_bubble_velocity) * (np.sqrt(self.base_lf) - np.sqrt(self.crit_lf_min))
        arg_steadystate = numerator/denominator
        coefficient = 1./np.sqrt(self.dimless_bubble_velocity)
        arcoth = .5 * mt.log(  (1 + arg_steadystate) / (arg_steadystate - 1)  )
        self.ss_height = coefficient * arcoth
        
    def Bubbling_SS_Prof(self):
        #calculate steady state equilibrium profile
        '''
        Below is inverse of Eqn 8 H et al. evaluated at alpha_0 (base) to find constant ksi_a
        '''
        arg_epsilon = mt.sqrt(self.base_lf / self.dimless_bubble_velocity)
        epsilon_arcoth = mt.log(  (arg_epsilon + 1) / (arg_epsilon - 1)  ) / 2.
        epsilon = -epsilon_arcoth / mt.sqrt(self.dimless_bubble_velocity) #xi_a (since xi = 0 at base)
        
        '''
        Eqn 8 H et al., giving equilibrium profile
        '''
        self.prof_points = np.linspace(0., self.col_height, self.num_space_samples)
        arg_prof = np.sqrt(self.dimless_bubble_velocity) * (self.prof_points - epsilon)
        prof_cotanh = np.cosh(arg_prof) / np.sinh(arg_prof)
        self.eq_prof = self.dimless_bubble_velocity * prof_cotanh * prof_cotanh #(Dimensionless) Equilibrium Profile (array)
#------------------------------------------------------------------------------------------------------------

    
    '''
    Defines the colour gradient scheme
    This makes it such that at profiles plotted have fainter colours if they are older profiles
    '''
    
    def Plot_Colors(self):
        self.gradient = 5. #adjust gradient of colour scale and transparency of first line
        Blues = plt.get_cmap('Blues') #retrieves 'blues' sequential colour map
        cNorm  = colors.Normalize(vmin = 0, vmax = self.gradient + len(self.plot_times)) #normalizes plotting range for color-coding
        self.scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=Blues) #creates scalar to colour map using normalized scale and selected colour map
        
    def Color_Gradient(self, is_dimensional):
        
        for i in range (0, self.num_time_samples):
            colorVal = self.scalarMap.to_rgba(self.gradient)
            if is_dimensional:
                plt.plot(self.phifactor * self.profiles[:, i], self.length_scale * self.plot_cells, color = colorVal)
            else:
                plt.plot(self.profiles[:, i], self.plot_cells, color = colorVal)
            self.gradient += 1

#------------------------------------------------------------------------------------------------------------
            
    def Dimless_Profile_Plot(self):
        
        if self.bubble_velocity > 0.:
            plt.xlim(xmax = 0.005, xmin =0)
            plt.ylim(ymax = 60, ymin = 0)
            plt.plot(self.eq_prof, self.prof_points, color = 'r', label = 'Bubbling Eq. Profile', linestyle = '--') #plots theoretical equilibrium profile in red
            conv_title = 'Alpha Profiles for a Rising Column \n' r'of Gas Velocity, $\nu=$' + '{:.6f}'.format(self.dimless_bubble_velocity)
        if self.input_lf == 0.:
            plt.plot(self.dimless_free_eq_prof, np.linspace(0.0, self.col_height, self.num_space_samples), linestyle = '--', color = 'g', label = 'Free Drainage Eq. Profile')
            if self.bubble_velocity == 0.:
                conv_title = 'Alpha Profiles for a Column under Free Drainage'
        else:
            conv_title = 'Alpha Profiles for a Column under Forced Drainage \n' r'of Amplitude, $\alpha$ = ' + '{:.4f}'.format(self.input_lf)
            
        plt.plot(self.crit_lf_line_lf_min, self.crit_lf_line_z, color = 'b', label = 'Rupture Liquid Fraction (Minimum)', linestyle = '--') #plots critical liquid fraction in blue
        plt.plot(self.crit_lf_line_lf_max, self.crit_lf_line_z, color = 'yellow', label = 'Rupture Liquid Fraction (Maximum)', linestyle = '--') #plots critical liquid fraction in blue
        
        plt.xlabel(r'$\alpha$')
        plt.ylabel('Height, $z$')
        plt.grid(b=True, which='major', linestyle='--')
        plt.title(conv_title)
        plt.legend(loc = 'best')
        plt.figure()
        
    def Dimless_Height_Plot(self):
        
        plt.title('Column Height against Time (Dimensionless)')
        plt.ylabel(r'Foam Height, $h$')
        plt.xlabel(r'Time Elapsed, $\tau$')
        plt.xlim(xmax = self.runtime, xmin =0)
        plt.grid(b=True, which='major', linestyle='--')
        plt.plot(self.plot_times, self.heights,label = 'Simulated Heights with crit. top = {:.3f}'.format(self.crit_lf_max) ) #height against time (for fixed velocity)

        if self.input_lf == 0. and self.dimless_bubble_velocity == 0.:
            plt.plot(np.linspace(0, self.runtime, 100), np.ones(100)*self.dimless_free_drainage_eq_height_min, color = 'r', linestyle = '--', label = 'Theoretical Steady State Height (Min Const Crit Lf)') #theoretical steady state height
            plt.plot(np.linspace(0, self.runtime, 100), np.ones(100)*self.dimless_free_drainage_eq_height_max, color = 'yellow', linestyle = '--', label = 'Theoretical Steady State Height (Max Const Crit Lf)')
            plt.ylim(ymax = 1.3 * max(self.dimless_free_drainage_eq_height_max, self.highest_point), ymin = 0)
            plt.legend(loc='best')
        elif self.bubble_velocity != 0. and self.dimless_bubble_velocity < self.crit_lf_min:
            plt.plot(np.linspace(0, self.runtime, 100), np.ones(100)*self.ss_height, color = 'r', linestyle = '--', label = 'Theoretical Steady State Height') #theoretical steady state height
            plt.ylim(ymax = 1.3 * max(self.ss_height, self.highest_point), ymin = 0)
            plt.legend(loc='best')
        else:
            plt.ylim(ymax = 1.3 * self.highest_point, ymin = 0)
        print ("Dimensionless Bubble Velocity: ", self.dimless_bubble_velocity)
        print ("Dimensionless crit liquid fraction: ", self.crit_lf_min)
        print ("Steady State Height: " , np.mean(self.heights[-10:]))
        print ("Dimensionless Final Height: ", self.heights[-1])
        print ("Dimensionless Initial Height: ", self.heights[0])
        print ("Slope Velocity: ", ((self.heights[-1]-self.heights[0])/self.runtime))
        if self.dimensional_plots:
            plt.figure()    
    
    def Dimless_Liquid_Height_Plot(self):

        plt.title('Liquid Drained against Time (Dimensionless and Normalised)')
        plt.ylabel(r'Normalised Liquid Drained, $h$')
        plt.xlabel(r'Time Elapsed, $\tau$')
        plt.xlim(xmax = self.runtime, xmin =0)
        plt.ylim(ymin = -0.00001)
        plt.grid(b=True, which='major', linestyle='--')
        plt.plot(self.plot_times, (self.height_increase )/(self.Total_Liquid_In_Foam ),label = 'Normalised Liquid Height') #height against time (for fixed velocity)
        #plt.plot(np.linspace(0, self.runtime, 100), np.ones(100)*self.Equilibrium_Avg_lf, color = 'r', linestyle = '--', label = 'Theoretical End behaviour')
        plt.legend(loc = 'best')
        if self.dimensional_plots:
            plt.figure()

#------------------------------------------------------------------------------------------------------------
            
    def Plot_Dimensionless(self):
        self.Plot_Colors()
        self.Color_Gradient(False)
        self.Dimless_Profile_Plot()
        self.Dimless_Height_Plot()
        self.Dimless_Liquid_Height_Plot()
        
#------------------------------------------------------------------------------------------------------------
    
    def Dimful_Profile_Plot(self):

        if self.bubble_velocity != 0.:
            plt.plot(self.phifactor * self.eq_prof, self.length_scale * self.prof_points, color = 'r', label = 'Bubbling Eq. Profile', linestyle = '--') #plots theoretical equilibrium profile in red
            conv_title = 'Liquid Fraction Profiles for a Rising Column \n' r'of Gas Velocity, $V=$' + '{:.6f}'.format(self.bubble_velocity) + r'$ms^{-1}$'
        if self.input_lf == 0.:
            plt.plot(self.free_eq_prof, np.linspace(0.0, self.length_scale * self.col_height, self.num_space_samples), linestyle = '--', color = 'g', label = 'Free Drainage Eq. Profile')
            if self.bubble_velocity == 0.:
                conv_title = 'Liquid Fraction Profiles for a Column under Free Drainage'
        else:
            conv_title = 'Liquid Fraction Profiles for a Column under Forced Drainage \n' r'of Amplitude, $\phi_l$ = ' + '{:.4f}'.format(Con.input_lf)
        
        plt.plot(self.phifactor * self.crit_lf_line_lf_min, self.length_scale * self.crit_lf_line_z, color = 'b', label = 'Rupture Liquid Fraction (Minimum)', linestyle = '--') #plots critical liquid fraction in blue
        plt.plot(self.phifactor * self.crit_lf_line_lf_max, self.length_scale * self.crit_lf_line_z, color = 'yellow', label = 'Rupture Liquid Fraction (Maximum)', linestyle = '--')
        
        plt.xlabel(r'Liquid Fraction, $\phi_l$')
        plt.ylabel(r'Height, $Z$ ($m$)')
        plt.grid(b=True, which='major', linestyle='--')
        plt.title(conv_title)
        plt.legend(loc = 'best')
        plt.figure()

    def Dimful_Height_Plot(self):

        plt.title('Column Height against Time')
        plt.ylabel('Foam Height, $H$ ($m$)')
        plt.xlabel('Time Elapsed, $T$ ($s$)')
        plt.xlim(xmax = self.time_scale * self.runtime, xmin = 0)
        plt.grid(b=True, which='major', linestyle='--')
        plt.plot(self.time_scale * self.plot_times, self.length_scale * self.heights,label = 'Simulated Heights') #height against time (for fixed velocity)

        if self.input_lf == 0. and self.dimless_bubble_velocity == 0.:
            plt.plot(np.linspace(0, self.runtime, 100), np.ones(100)*self.free_drainage_eq_height_min, color = 'r', linestyle = '--', label = 'Theoretical Steady State Height (Min Const Crit lf)') #theoretical steady state height
            plt.plot(np.linspace(0, self.runtime, 100), np.ones(100)*self.free_drainage_eq_height_max, color = 'yellow', linestyle = '--', label = 'Theoretical Steady State Height (Max Const Crit lf)')
            plt.ylim(ymax = 1.3 * max(self.free_drainage_eq_height_min, self.length_scale * self.highest_point), ymin = 0)
            plt.legend(loc='best')
        elif self.bubble_velocity != 0. and self.dimless_bubble_velocity < self.crit_lf_min:
            plt.plot(np.linspace(0, self.time_scale * self.runtime, 100), np.ones(100) * self.length_scale * self.ss_height, color = 'r', linestyle = '--', label = 'Theoretical Steady State Height') #theoretical steady state height
            plt.ylim(ymax = 1.3 * self.length_scale * max(self.ss_height, self.highest_point), ymin = 0)
            plt.legend(loc='best')
        else:
            plt.ylim(ymax = 1.3 * self.length_scale * self.highest_point, ymin = 0)
        print ("Dimensional Bubbling Velocity: ", self.bubble_velocity)
        print ("Steady State Height: " , self.length_scale * np.mean(self.heights[-10:]))
        print ("Dimensional Final Height: ", self.length_scale * self.heights[-1])
        print ("Dimensional Initial Height: ", self.length_scale * self.heights[0])
        print ("Slope Velocity: ", ((self.heights[-1]-self.heights[0])/self.runtime)*self.length_scale/self.time_scale)
        plt.figure()
    
    def Dimful_Liquid_Height_Plot(self):

        plt.title('Liquid Drained against Time (Dimensionful and Height Normalised)')
        plt.ylabel(r' Normalised Liquid Drained, $h$')
        plt.xlabel(r'Time Elapsed, $t$ ($s$)')
        plt.xlim(xmax = self.time_scale*self.runtime, xmin =0)
        plt.ylim(ymin = -0.00001)
        plt.grid(b=True, which='major', linestyle='--')
        plt.plot(self.time_scale*self.plot_times2, (self.height_increase )/(self.Total_Liquid_In_Foam),label = 'Normalised Liquid Height') #height against time (for fixed velocity)
        #plt.plot(np.linspace(0, self.runtime*self.time_scale, 100), np.ones(100)*self.Equilibrium_Avg_lf, color = 'r', linestyle = '--', label = 'Theoretical End behaviour')
        plt.legend(loc = 'best')
        plt.figure()

    def Theoretical_SSH(self):
        self.theoretical_height_array = []
        self.bubble_vel = 0.0
        #self.bubble_vel = np.linspace(0, self.dimensional_crit_velocity, 1001)
        for i in range (0, 1000):
            if (self.bubble_vel == 0.0):
                self.theoretical_height = 1 / np.sqrt(self.crit_lf_min) - 1 / np.sqrt(self.base_lf)
                #self.theoretical_height2 = self.length_scale * self.theoretical_height
            else:
                numerator1 = np.sqrt(self.crit_lf_min * self.base_lf) - self.bubble_vel
                denominator1 = np.sqrt(self.bubble_vel) * (np.sqrt(self.base_lf) - np.sqrt(self.crit_lf_min))
                arg_steadystate1 = numerator1/denominator1
                coefficient1 = 1./np.sqrt(self.bubble_vel)
                #arcoth1 = np.sinh(arg_steadystate1) / np.cosh(arg_steadystate1)
                arcoth1 = .5 * mt.log(  (1 + arg_steadystate1) / (arg_steadystate1 - 1)  )
                self.theoretical_height = coefficient1 * arcoth1
                #self.theoretical_height2 = self.length_scale * self.theoretical_height
            i += 1
            self.bubble_vel += self.dimless_crit_velocity / 1000.
            self.theoretical_height_array.append(self.theoretical_height)

    def Steady_State_Height_vs_Bubble_Velocity(self):
        self.bubble_vel = np.linspace(0, self.dimless_crit_velocity, 1000)
        self.critical_height = np.linspace(0, 100, 1000)
        self.simulated_bubble_velocity = [0.0000090500085, 0.00009050085, 0.00036200034, 0.0004525004, 0.0005882505530445465, 0.0006787506381283229, 0.0008145007657539876, 0.000905000850837764, 0.0011312510635472048 , 0.0013575012762566457, 0.0014932514038823103, 0.001810001701675528 , 0.0019005017867593044, 0.001991001871843081, 0.0020000518803514585 , 0.002009101888859836, 0.0020136268931140247]
        self.simulated_steady_state_heights = [20.6, 20.9, 22.1, 22.5, 23.2, 23.8, 24.6, 25.3, 27.2, 29.8, 31.7, 39.7, 45.2, 58, 61.2, 66.3, 71.3]
        plt.title('Steady State Height vs Bubble Velocity')
        plt.xlabel('Bubble Velocity')
        plt.ylabel('Steady State Height')
        plt.ylim(ymax = 90)
        plt.grid(b=True, which='major', linestyle='--')
        plt.plot(np.ones(1000) * self.dimless_crit_velocity, self.critical_height, 'g', linestyle = '--', label = 'Theoretical Critical Velocity')
        plt.plot(self.bubble_vel, self.theoretical_height_array, 'r', linestyle = '--', label = 'Theoretical Prediction')
        #plt.plot(self.dimless_bubble_velocity, np.mean(self.heights[-10:]), 'X', label = 'Calculated Height')
        plt.plot(self.simulated_bubble_velocity, self.simulated_steady_state_heights, 'bX', label  = 'Calculated Heights')
        plt.legend(loc = 'best')
        plt.figure()
        print (self.dimless_bubble_velocity)
        print (self.heights[-1:])
        print (self.bubble_vel[-1:])

    def Steady_State_Height_vs_Critical_Liquid_Fraction(self):
        self.critical_liquid_fraction = [0.02, 0.015, 0.01, 0.005, 0.004, 0.003, 0.001, 0.00075, 0.0005, 0.00025, 0.0002]
        #self.critical_liquid_fraction = [i * 0.5 for i in self.critical_liquid_fraction]
        self.steady_height = [10.9, 12.3, 14.4, 18.8, 21, 22.8, 33.7, 38.8, 42.2, 54.4, 63]
        self.critical_liquid_fraction_top = np.linspace(0.0001, 0.02, 1000)
        self.steady_state_height_theory = 1 / np.sqrt(self.critical_liquid_fraction_top)
        plt.title('Steady State Height vs Critical Liquid Fraction')
        plt.xlabel('Critical Liquid Fraction')
        plt.ylabel('Steady State Height')
        plt.plot(self.critical_liquid_fraction, self.steady_height, 'bX', label = 'Calculated Heights')
        #plt.plot(Con.crit_lf_min, self.heights[-1:], 'bX', label = 'Calculated Heights')
        plt.plot(self.critical_liquid_fraction_top, self.steady_state_height_theory, 'r--', label = 'Theoretical Result')
        plt.legend(loc = 'best')
    
#------------------------------------------------------------------------------------------------------------

    def Plot_Dimensional(self):
        self.Plot_Colors()
        self.Color_Gradient(True)
        self.Dimful_Profile_Plot()
        self.Dimful_Height_Plot()
        self.Dimful_Liquid_Height_Plot()
        self.Steady_State_Height_vs_Bubble_Velocity()
        self.Steady_State_Height_vs_Critical_Liquid_Fraction()
#------------------------------------------------------------------------------------------------------------
        
start_time = datetime.now()
Instance = PlotClass()
end_time = datetime.now()
print ('Duration: {}'.format(end_time - start_time))
plt.show()
