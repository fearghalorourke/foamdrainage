import numpy as np
import math as mt #very sloppy
import io, libconf
from datetime import datetime

#------------------------------------------------------------------------------------------------------------

class Test_Class():
    
    '''
    Class performs calculations of relevant parameters 
    in order to find the end behaviour and max. allowed input values 
    '''
    
    def __init__(self):
        
        self.Variables()
        self.Scaling()
        self.Make_Dimensionless()
        self.Calculate_Intermediate_Variables()
        self.Calculate_Free_Drainage_Equilibrium_Height()
        self.Calculate_Critical_Velocity()
        if self.bubble_velocity > 0.:
            self.Calculate_Steady_State_Height()
        self.Calculate_Height_For_Small_Velocities()
        
#------------------------------------------------------------------------------------------------------------

    def Variables(self):
    
        with io.open('config.cfg') as Conf_file:
            Con = libconf.load(Conf_file)
            
        self.initial_lf = Con.initial_lf
        self.base_lf = Con.base_lf
        self.input_lf = Con.input_lf
        self.crit_lf = Con.crit_lf_max
        
        self.bubble_velocity = Con.bubble_vel
        self.in_flow = self.input_lf**2 

        self.C_factor = Con.C_factor
        self.f_factor = Con.f_factor
        
        self.gamma = Con.gamma
        self.gravity = Con.gravity
        self.water_density = Con.water_density
        self.water_viscosity = Con.water_viscosity
        self.effective_viscosity = 3.*self.f_factor*self.water_viscosity

        self.bubdiam = Con.bubdiam
        
    def Scaling(self):
        
        self.length_scale = mt.sqrt((self.C_factor*self.gamma)/(self.water_density*self.gravity))
        self.time_scale = self.effective_viscosity*(1./mt.sqrt(self.C_factor*self.gamma*self.water_density*self.gravity))
        self.phifactor = (5.35*self.length_scale*self.length_scale)/pow( ((4./3.)*mt.pi*(self.bubdiam * 0.5)*(self.bubdiam * 0.5)*(self.bubdiam * 0.5)), 2./3. )
        
    def Make_Dimensionless(self):
        
        self.initial_lf = self.initial_lf/self.phifactor
        self.base_lf = self.base_lf/self.phifactor
        self.input_lf = self.input_lf/self.phifactor
        self.crit_lf = self.crit_lf/self.phifactor
        self.dimless_bubble_velocity = self.bubble_velocity * self.time_scale / self.length_scale

#------------------------------------------------------------------------------------------------------------
        
    def Calculate_Intermediate_Variables(self):
        
        self.pg = self.water_density*self.gravity
        self.fn = self.f_factor*self.water_viscosity
        self.vol_factor = mt.pow(mt.pi*mt.pow(self.bubdiam, 3)/6, 2./3.)/5.35
        
    def Calculate_Steady_State_Height(self):
        
        if self.bubble_velocity < self.velocity_crit:
            numerator = np.sqrt(self.crit_lf * self.base_lf) - self.dimless_bubble_velocity
            denominator = np.sqrt(self.dimless_bubble_velocity) * (np.sqrt(self.base_lf) - np.sqrt(self.crit_lf))
            arg_steadystate = numerator/denominator
            coefficient = 1./np.sqrt(self.dimless_bubble_velocity)
            arcoth = .5 * mt.log(  (1 + arg_steadystate) / (arg_steadystate - 1)  )
            ss_height = coefficient * arcoth
            
            print("\nDimensional Bubbling Steady State Height: " + str(self.length_scale * ss_height) + ' m')
            print("Dimensionless Bubbling Steady State Height: " + str(ss_height))
        
        else:
            print("\nHeight Diverges")
            
    def Calculate_Free_Drainage_Equilibrium_Height(self):
        
        free_drainage_eq_height = 2*self.gamma/(np.sqrt(3)*self.bubdiam*self.pg)*(1/np.sqrt(self.phifactor * self.crit_lf) - 1/np.sqrt(self.phifactor * self.base_lf))
        dimless_free_drainage_eq_height = free_drainage_eq_height / self.length_scale
        
        print("\nDimensional Free Drainage Steady State Height: " + str(free_drainage_eq_height) + ' m')
        print("Dimensionless Free Drainage Steady State Height: " + str(dimless_free_drainage_eq_height))
        
    def Calculate_Critical_Velocity(self):
    
        self.dimless_velocity_crit = self.crit_lf
        self.velocity_crit = self.length_scale / self.time_scale * self.dimless_velocity_crit
        
        print("\nDimensional Critical Velocity: " + str(self.velocity_crit) + " m/s")
        print("Dimensionless Critical Velocity: " + str(self.dimless_velocity_crit))
        
    def Calculate_Height_For_Small_Velocities(self):
        
        dimless_height_smallv = 1/np.sqrt(self.crit_lf) - 1/np.sqrt(self.base_lf)
        height_smallv = self.length_scale * dimless_height_smallv
        
        print("\nDimensional Small Velocity Height: " + str(height_smallv) + " m")
        print("Dimensionless Small Velocity Height: " + str(dimless_height_smallv))
        
#------------------------------------------------------------------------------------------------------------

start_time = datetime.now()
Test_Class()
end_time = datetime.now()
print('\nRunning Duration: {}'.format(end_time - start_time))
