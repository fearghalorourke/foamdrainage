import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
import io, libconf
import scipy.optimize as sco

with io.open('config.cfg') as Conf_file:
        Con = libconf.load(Conf_file)
        
#----------------------------------------------------------------------------------------------------

class Model_Height:
    
    length_scale = np.sqrt((Con.C_factor*Con.gamma)/(Con.water_density*Con.gravity))

    top_data = np.linspace(0.0005,0.016,32)
    SSH_data = np.array([39.1,30.4,26.1,23.4,21.4,
                        19.9,18.7,17.7,16.9,16.2,
                        15.5,15.,14.5,14.1,13.6,
                        13.3,12.9,12.6,12.3,12.1,
                        11.8,11.6,11.4,11.2,11.,
                        10.8,10.6,10.5,10.3,10.2,
                        10.0,9.9])
    
    Fake_SSH_Data = np.linspace(0.,100,10000)

    def __init__(self):
        self.E_Crit_Lf = self.Effective_crit_lf(Model_Height.SSH_data)
        #self.Plot_Figure_1()
        #self.Plot_Figure_2()
        #self.Plot_Figure_3()
    
    Effective_crit_lf = lambda self, Height:( 
        pow((1./np.sqrt(Con.base_lf)) + (((Height*Model_Height.length_scale)/(2.*Con.gamma))*np.sqrt(3.)*Con.bubdiam*Con.water_density*Con.gravity),-2))


    def Plot_Figure_1(self,fig):
        
        plt.plot(Model_Height.top_data,Model_Height.SSH_data,'bo',label = 'The Dimensionless SSH against the\n Crit. Lf. at top of Column',figure = fig)
        plt.xlabel('Critical Lf. at the Top of Column')
        plt.ylabel('Steady State Height')
        plt.plot(0.0001,69.8,'bo',figure = fig)
        plt.plot(0.0002,54.,'bo',figure = fig)
        plt.legend(loc = 'best')
        plt.grid(b=True, which='major', linestyle='--',figure = fig)

    def Plot_Figure_2(self):
        
        fig_2 = plt.figure()
    
        plt.plot(self.Effective_crit_lf(Model_Height.Fake_SSH_Data),Model_Height.Fake_SSH_Data,'--y', figure = fig_2)
        plt.plot(self.E_Crit_Lf,Model_Height.SSH_data,'bo', label = 'Effective Constant Critical Liquid Fraction\n required for each height')
        plt.xlabel('\'Effective\' Critical Liquid Fraction')
        plt.ylabel('Steady State Height')
        plt.legend(loc = 'best')
        plt.grid(b=True, which='major', linestyle='--',figure = fig_2)

    def Plot_Figure_3(self):
        
        fig_3 = plt.figure()
    
        plt.plot(Model_Height.top_data,self.E_Crit_Lf,'go',label = 'The \'Effective\' Critical Liquid Fraction\n vs. the value at top of column',figure = fig_3)
        plt.plot(0.0001,self.Effective_crit_lf(69.8),'go')
        plt.xlabel('Critical Liquid Fraction at top of the column')
        plt.ylabel('The \'Effective\' Critical Liquid Fraction using the old model')
        plt.legend(loc = 'best')
        plt.grid(b=True, which = 'major', linestyle = '--', figure = fig_3)

#----------------------------------------------------------------------------------------------------
Plot =Model_Height()    

class Fitting_Class:
    
    SSH_data = np.hstack((69.8,54.,Model_Height.SSH_data))
    top_data = np.hstack((.0001,0.0002,Model_Height.top_data))
    Effective_data = np.hstack((Plot.Effective_crit_lf(69.8),Plot.Effective_crit_lf(54.),Plot.E_Crit_Lf))
    
    def __init__(self,Input):
        if Input == 1:
            self.Make_A_Canvas_1_Fit()
        elif Input == 3:
            self.Make_A_Canvas_3_Fit()
        
    Fit_1 = lambda self, x, a, b, c : a*pow(b+x,-1./2.) + c 

    Fit_3 = lambda self, x, a, b , c ,d : a*pow(x+b,d) + c
        
    
    def Make_A_Canvas_1_Fit(self):

        
        fig = plt.figure()
        
        plt.subplots_adjust(bottom=0.3)
        
        self.a = 0.
        self.b = 0.
        self.c = 0.
        
        
        plt.plot(Fitting_Class.top_data,Fitting_Class.SSH_data,'ro',figure = fig)
        self.Plotted, = plt.plot(Fitting_Class.top_data,self.Fit_1(Fitting_Class.top_data,self.a,self.b,self.c),'--k')
        
        axtime_a = plt.axes([.05,.2, 0.65, 0.03]) 
        axtime_b = plt.axes([.05,.15, 0.65, 0.03])
        axtime_c = plt.axes([.05,.1,0.65,.03])
        
        
        self.a_Slider = Slider(axtime_a, 'a', -10.,10., valinit=0.0)
        self.b_Slider = Slider(axtime_b, 'b', -2*min(Fitting_Class.top_data), 2*min(Fitting_Class.top_data), valinit=0.0)
        self.c_Slider = Slider(axtime_c, 'c', -10.*max(Fitting_Class.SSH_data), 10.*max(Fitting_Class.SSH_data), valinit=0.0)
        
        
        def update_a(val):
            self.a = (self.a_Slider.val) #Function chooses image number to display using the slider
            
            self.Plotted.set_ydata(self.Fit_1(Fitting_Class.top_data,self.a,self.b,self.c))
            fig.canvas.draw()
    
        def update_b(val):
            self.b = (self.b_Slider.val)
            
            self.Plotted.set_ydata(self.Fit_1(Fitting_Class.top_data,self.a,self.b,self.c))
            fig.canvas.draw()
    
        def update_c(val):
            self.c = (self.c_Slider.val)
            
            self.Plotted.set_ydata(self.Fit_1(Fitting_Class.top_data,self.a,self.b,self.c))
            fig.canvas.draw()


            
        self.a_Slider.on_changed(update_a)
        self.b_Slider.on_changed(update_b)
        self.c_Slider.on_changed(update_c)

    
    def Make_A_Canvas_3_Fit(self):
        
        fig = plt.figure()
        
        plt.subplots_adjust(bottom=0.3)
        
        self.a = 0.
        self.b = 0.
        self.c = 0.
        self.d = 0.
        
        plt.plot(Fitting_Class.top_data,Fitting_Class.Effective_data,'ro',figure = fig)
        self.Plotted, = plt.plot(Fitting_Class.top_data,self.Fit_3(Fitting_Class.top_data,self.a,self.b,self.c,self.d),'--k')
        
        axtime_a = plt.axes([.05,.2, 0.65, 0.03]) 
        axtime_b = plt.axes([.05,.15, 0.65, 0.03])
        axtime_c = plt.axes([.05,.1,0.65,.03])
        axtime_d = plt.axes([.05,.05,0.65,.03])
        
        self.a_Slider = Slider(axtime_a, 'a', -10.*max(Fitting_Class.Effective_data),10.*max(Fitting_Class.Effective_data), valinit=0.0)
        self.b_Slider = Slider(axtime_b, 'b', -2*min(Fitting_Class.top_data), 2*min(Fitting_Class.top_data), valinit=0.0)
        self.c_Slider = Slider(axtime_c, 'c', -10.*max(Fitting_Class.Effective_data), 10.*max(Fitting_Class.Effective_data), valinit=0.0)
        self.d_Slider = Slider(axtime_d, 'd', -2.,2., valinit=0.0)
        
        def update_a(val):
            self.a = (self.a_Slider.val) #Function chooses image number to display using the slider
            
            self.Plotted.set_ydata(self.Fit_3(Fitting_Class.top_data,self.a,self.b,self.c,self.d))
            fig.canvas.draw()
    
        def update_b(val):
            self.b = (self.b_Slider.val)
            
            self.Plotted.set_ydata(self.Fit_3(Fitting_Class.top_data,self.a,self.b,self.c,self.d))
            fig.canvas.draw()
    
        def update_c(val):
            self.c = (self.c_Slider.val)
            
            self.Plotted.set_ydata(self.Fit_3(Fitting_Class.top_data,self.a,self.b,self.c,self.d))
            fig.canvas.draw()

        def update_d(val):
            self.d = (self.d_Slider.val)
            
            self.Plotted.set_ydata(self.Fit_3(Fitting_Class.top_data,self.a,self.b,self.c,self.d))
            fig.canvas.draw()
            
        self.a_Slider.on_changed(update_a)
        self.b_Slider.on_changed(update_b)
        self.c_Slider.on_changed(update_c)
        self.d_Slider.on_changed(update_d)

    
Best_Fit = Fitting_Class(1)

plt.show()
fig_1 = plt.figure()

(a, b, c), pcov_T = sco.curve_fit(Best_Fit.Fit_1,Fitting_Class.top_data,Fitting_Class.SSH_data,[Best_Fit.a, Best_Fit.b, Best_Fit.c])
plt.plot(np.linspace(0.000000000000001,0.02,1000), Best_Fit.Fit_1(np.linspace(0.,0.02,1000), a, b, c),'--k',figure = fig_1)
print('{0}'.format((a,b,c)))


Plot.Plot_Figure_1(fig_1)
Plot.Plot_Figure_3()
Plot.Plot_Figure_2()
plt.show()
