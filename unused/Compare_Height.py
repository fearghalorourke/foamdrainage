import matplotlib.pyplot as plt
import numpy as np
import io, libconf

with io.open('config.cfg') as Conf_file:
    Con = libconf.load(Conf_file)
        
fig_1 = plt.figure()

Var = np.load('Plot_Heights_Array.npy')
Const = np.load('Plot_Heights_Array_const.npy')

Number_of_steps_var = Var.size
Number_of_steps_const = Const.size

maxtime = int(Con.Dimless_runtime/Con.dt)

plot_times_var = Con.dt*np.linspace(0.,maxtime,Number_of_steps_var)
plot_times_const = Con.dt*np.linspace(0.,maxtime,Number_of_steps_const)

plt.plot(plot_times_const,Const,'--b', label = 'Heights vs. time for \'effective\' crit lf',figure =fig_1 )
plt.plot(plot_times_var,Var,'--r',label = 'Heights vs. Time with varying crit lf',figure = fig_1 )
plt.grid(b=True, which='major', linestyle='--',figure = fig_1)
plt.legend(loc = 'best')

plt.show()
